
#LU 6
#0

tupple=("a","b","c")

#In REPL type

tupple[0]
tupple[1]
tupple[2]

#1 you cant change a tuple!
# variable[0]=5 only works if variable is a list

#2 

aapl_prices=(2.3, 2.4, 3.5, 2.9, 3.4)

#3 sort a tuple is NOT POSSIBLE

#4

aapl_prices_list=[2.3, 2.4, 3.5, 2.9, 3.4]

aapl_prices_list.append(4.6)

#5 +#6
#tupple: you cannot change it, append it or sort it. a tupple stores values but cannot be changed

#list: stores values, can be sorted, you can change value and add new ones

# a list is only equal to a list, a tupple only equal to a tuple


#7 change () for [] !!

#8 values:returns the values associated with each key, in the same order 
# keys: returns the keys in the dictionary by order 
# items: returns the pairs key-value


#9

dict_9={}

dict_9["hello"]="world"

dict_9 ["abc"]=123

#10

stock_prices={
        "APPL":100,
        "GOOGL":99,
}

stock_of_interest="APPL"

print(stock_prices[stock_of_interest])

#11 initialize a var with syntax or define in a function

#12 

stock_prices={
        "APPL":100,
        "GOOGL":99,
}

del stock_prices["GOOGL"]

stock_prices["def"]=30

stock_prices

stock_prices.pop("def")

stock_prices


#13

def a_list_fct(list_1):
    list_1.append("homie")
    return list_1

#14
    
def another_fct(a,b,c,d,e,f,g):
    return (a+b+c+d+e+f+g)/7
    
def another_list_fct(list_2):
    return sum(list_2)/len(list_2)
